const bcrypt = require('bcrypt')
const SALT_ROUNDS = 10

const jwt = require('jsonwebtoken')
const dotenv = require('dotenv')
dotenv.config()
const TOKEN_SECRET = process.env.TOKEN_SECRET

const mysql = require('mysql')


const connection = mysql.createConnection({
    host : 'localhost',
    user: 'music_admin',
    password: 'music_admin',
    database: 'MusicSchoolSystem' 
})

connection.connect()

const express = require('express')
const app = express()
const port= 4000

/*Middle WARE*/

function authenicateToken(req,res,next){
    const authHeader = req.headers['authorization']
    const token = authHeader && authHeader.split('')[1]
    if (token == null) return res.sendStatus(401)
    jwt.verify(token,TOKEN_SECRET,(err,user) =>{
        if(err) {
            //console.log(err)
            return res.sendStatus(403)
        }
        else{
            req.user = user
            next()
        }
    })
}



/*LOG IN */ 

app.post("/login", (req,res)=>{

    let username = req.query.username
    let user_password = req.query.password
    let query = `SELECT * FROM Student WHERE Username = '${username}'`

        connection.query( query,(err, rows) => {
            if (err){
                console.log(err)
                res.json({
                    "status" : "400",
                    "message" : "Error inserting data into db"

                })
            }else {
                let db_password = rows[0].Password
                bcrypt.compare(user_password, db_password,(err,result) =>{
                    if(result){
                        let payload = {
                            "username" : rows[0].Username,
                            "user_id" : rows[0].StudentID,
                            "IsAdmin": rows[0].IsAdmin
                        }
                        
                        console.log(payload)
                        let token = jwt.sign(payload, TOKEN_SECRET,{expiresIn : '1d'})
                        res.send(token)

                    }else {
                        
                        res.send("Invalid username / password")}
                })
                
            }
        }); 
    


})











/* API for register*/

app.post("/register",(req,res) => {
    let student_name = req.query.student_name
    let student_surname = req.query.student_surname
    let student_username = req.query.student_username
    let student_password = req.query.student_password

    bcrypt.hash(student_password , SALT_ROUNDS,(err,hash) =>{

        let query =  `INSERT INTO Student
                        (StudentName,StudentSurname,Username,Password,IsAdmin)
                            VALUES('${student_name}','${student_surname}',
                            '${student_username}','${hash}',false)`

                        console.log(query)

    connection.query(query,(err,rows) =>{

        if(err){
            console.log(err)
            res.json({
                "status" : "400",
                "message" : "Error"
            })
        }else {
            res.json({
                "status" : "200",
                "message" : "Register Success!"
            })
                
        }
    });

    })



    



})





/* List of The Music Course*/ 
app.get("/list_course",(req,res) =>{
    let query =  "SELECT * from MusicCourse";
    connection.query(query,(err,rows) =>{

        if(err){
            res.json({
                "status" : "400",
                "message" : "Error"
            })
        }else {
            res.json(rows)
        }
    });
})

/*List of all student*/
app.get("/list_student",(req,res) =>{
    let query =  "SELECT * from Student";
    connection.query(query,(err,rows) =>{

        if(err){
            res.json({
                "status" : "400",
                "message" : "Error"
            })
        }else {
            res.json(rows)
        }
    });
})

/* Add Course */

app.post("/add_music_course",authenicateToken,(req,res) =>{
    let course_name = req.query.course_name;
    let course_price = req.query.course_price;
    let query =  `INSERT INTO MusicCourse 
                    (CourseName,CoursePrice)
                    VALUES ('${course_name}','${course_price}')`;


    connection.query(query,(err,rows) =>{

        if(err){
            res.json({
                "status" : "400",
                "message" : "Error"
            })
        }else {
            res.json({
                "status" : "200",
                "message" : "Success!"
            })
                
        }
    });
})

/*UPDATE COURSE*/

app.post("/update_music_course",(req,res) =>{
    let course_id = req.query.course_id;
    let course_price = req.query.course_price;
    let course_name = req.query.course_name;
    let query =  `UPDATE MusicCourse SET
                        CourseName = '${course_name}',
                        CoursePrice = ${course_price}
                        WHERE CourseID=${course_id}`

                        console.log(query)

    connection.query(query,(err,rows) =>{

        if(err){
            console.log(err)
            res.json({
                "status" : "400",
                "message" : "Error"
            })
        }else {
            res.json({
                "status" : "200",
                "message" : "Update Success!"
            })
                
        }
    });
})


/*Delete Course*/
app.post("/delete_music_course",(req,res) =>{
    let course_id = req.query.course_id;
    let query =  `DELETE FROM MusicCourse
                        WHERE CourseID=${course_id}`

                        console.log(query)

    connection.query(query,(err,rows) =>{

        if(err){
            console.log(err)
            res.json({
                "status" : "400",
                "message" : "Error"
            })
        }else {
            res.json({
                "status" : "200",
                "message" : "delete Success!"
            })
                
        }
    });
})



app.listen(port,() => {
    console.log(`Now start running system backend port ${port}`)
})


